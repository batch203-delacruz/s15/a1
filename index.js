// console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let myFirstName = "Daisyrie";
	console.log("First Name: " + myFirstName);

	let myLastName = "Dela Cruz";
	console.log("Last Name: " + myLastName);

	let myAge = 27;
	console.log("Age:" + 30);

	console.log("Hobbies:")

	let myHobbies = ["Singing", "Reading", "Playing"];
	console.log(myHobbies)

	console.log("Work Address:");

	let myWorkAddress = {
			houseNumber: "371",
			street: "Garnet St.",
			city: "Lipa",
			state: "Batangas"
		}
	console.log(myWorkAddress)



/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
	let firstName = "John";
	console.log("First Name: " + firstName);
	
	let lastName = "Smith";
	console.log("Last Name: " + lastName);

	let age = 30;
	console.log("Age:" + 30);

	console.log("Hobbies:")

	let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
	console.log(hobbies)

	console.log("Work Address:");

	let workAddress = {
			houseNumber: "32",
			street: "Washington",
			city: "Lincoln",
			state: "Nebraska"
		}
	console.log(workAddress)

	let fullName = "Steve Rogers";
	console.log("My full name is:" + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);

	console.log("My Friends are:");
	let friends = ["Tony","Bruce","Thor", "Natasha","Clint","Nick"];
	console.log(friends)

	console.log("My Full Profile: ");
	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
	}
	console.log(profile)

	let bfName = "Bucky Barnes";
	console.log("My bestfriend is: " + bfName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

